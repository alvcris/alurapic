import Cadastro from './components/cadastro/Cadastro.vue'
import Home from './components/home/Home.vue'
import GitHub from './components/github/GitHub.vue'
import Task from './components/task/Task.vue'

export const routes = [ 
    {
        path: '/', name: 'home', component: Home, menu: 'Home', isMenu: true
    },
    {
        path: '/cadastro', name: 'cadastro', component: Cadastro, menu: 'Cadastro', isMenu: true
    },
    {
        path: '/cadastro/:id', name: 'altera', component: Cadastro, menu: 'Cadastro', isMenu: false
    },
    {
        path: '/gitHub', name: 'github', component: GitHub, menu: 'GitHub', isMenu: true
    },
    {
        path: '/task', name: 'task', component: Task, menu: 'Task', isMenu: true
    }

    
];