/**
 * Inicialização do projeto
 * vue init webpack-simple alurapic
 * 
 * dependencias:
 * npm install vue-router --save
 * npm install axios vue-axios --save
 * npm install vue-resource --save
 * 
 */

//bootstrap 
// npm install bootstrap --save
// npm install jquery --save
// npm install popper.js --save
//
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap/dist/js/bootstrap.js'

import Vue from 'vue'
import App from './App.vue'
//import VueResource from 'vue-resource'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import { routes } from './routes'
import './directives/Transform'

//bootstrap
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'

// Buefy
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
Vue.use(Buefy);


// Axios
Vue.use(VueAxios, axios)
const axiosConfig = {
  baseURL: 'http://localhost:3000/'
};
Vue.prototype.$axios = axios.create(axiosConfig)

//Vue.use(VueResource)
//Vue.http.options.root = 'http://localhost:3000'


//Routes
Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
