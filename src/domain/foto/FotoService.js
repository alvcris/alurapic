export default class FotoService{     
    constructor(axios){
        //this._resource = resource('v1/fotos{/id}');
        this._axios = axios;
        this.RESOURCE = 'v1/alurapic/fotos/'; 
    }    

    async lista(){
        let response
        try {
           response = await this._axios.get(this.RESOURCE)
        } catch (error) {
            console.log("lista()")
            console.log(error)
        }

        return response.data
    }

    cadastra(foto){
        return this._axios.post(this.RESOURCE, foto)
        //return this._resource.save(foto)
    }

    apaga(foto){
        //return this._resource.delete({id: foto._id})
        return this._axios.delete(this.RESOURCE + foto._id)
    }

    async findOne(id){
        let response
        try {
           response = await this._axios.get(this.RESOURCE + id)
        } catch (error) {
            console.log("lista()")
            console.log(error)
        }

        return response.data
    }

    editar(foto){
        console.log('editando...')
        console.log(foto)
        return this._axios.put(this.RESOURCE + foto._id, foto)
        .then(response => response.data)
    }


}